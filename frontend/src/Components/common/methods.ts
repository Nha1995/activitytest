import { ITIMEINTERVAL } from "../../api/graphqlQueryTypes";

export const dateToString = (date: Date): string => {
  const day = date.getDate() < 10 ? `0${date.getDate()}` : date.getDate();
  const month = date.getMonth() + 1 < 10
    ? `0${date.getMonth() + 1}` : date.getMonth() + 1;
  return `${date.getFullYear()}-${month}-${day}`;
};

export const stringToDate = (dateStr: string): Date | null => {
  const splitStr = dateStr.split('-');
  if (!Number.isNaN(+splitStr[0]) && splitStr[0].length === 4
    && !Number.isNaN(+splitStr[1]) && splitStr[1].length === 2
    && !Number.isNaN(+splitStr[2]) && splitStr[2].length === 2) {
    const date = new Date(+splitStr[0], +splitStr[1] - 1, +splitStr[2]);
    if (date.getDate() === +splitStr[2] && +splitStr[0] > 1900 && +splitStr[0] < 2100
      && date.getMonth() === +splitStr[1] - 1) {
      return new Date(+splitStr[0], +splitStr[1] - 1, +splitStr[2]);
    }
  }
  return null;
};

export const removeTypenames = (timeIntervals: ITIMEINTERVAL[]): ITIMEINTERVAL[] => {
  return timeIntervals.map((item) => {
    return {
      id: item.id,
      userId: item.userId,
      dateRegistration: item.dateRegistration,
      dateLastActivity: item.dateLastActivity,
    } as ITIMEINTERVAL
  })
}

export const findSameUserId = (timeIntervals: ITIMEINTERVAL[]) => {
  const userIds = timeIntervals.map((item) => item.userId);
  let sorted_arr = userIds.slice().sort();
  let results: number[] = [];
  for (let i = 0; i < sorted_arr.length - 1; i++) {
    if (sorted_arr[i + 1] !== null && sorted_arr[i] !== null && sorted_arr[i + 1] === sorted_arr[i]) {
      results.push(sorted_arr[i]!);
    }
  }
  return results;
}
