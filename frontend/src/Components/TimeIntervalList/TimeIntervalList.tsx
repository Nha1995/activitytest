import React from 'react';
import { ITIMEINTERVAL } from "../../api/graphqlQueryTypes";
import cn from 'classnames';
import classes from './TimeIntervalList.module.scss';
import { nanoid } from 'nanoid';
import { findSameUserId } from '../common/methods';

interface Props {
  timeIntervals: ITIMEINTERVAL[]
  setTimeIntervals: React.Dispatch<React.SetStateAction<ITIMEINTERVAL[]>>
  sameUserIds: number[]
  setSameUserIds: React.Dispatch<React.SetStateAction<number[]>>
  dateErrorIndexes: number[]
  setDateErrorIndexes: React.Dispatch<React.SetStateAction<number[]>>
}

const TimeIntervalList = (props: Props) => {

  const removeTimeInterval = (index: number) => {
    const newTimeIntervals = props.timeIntervals.filter((item, i) => {
      return i !== index;
    });
    if (newTimeIntervals.length > 0) {
      props.setTimeIntervals(newTimeIntervals);
    }
  }

  const changeHandler = (
    fieldName: string,
    index: number,
    e: React.ChangeEvent<HTMLInputElement>
  ) => {
    switch (fieldName) {
      case 'userId':
        const newTimeIntervals = props.timeIntervals;
        newTimeIntervals[index].userId = e.target.value ? +e.target.value : null;
        props.setSameUserIds([...findSameUserId(newTimeIntervals)]);
        props.setTimeIntervals([...newTimeIntervals]);
        break;
      case 'dateRegistration':
        const changedTimeIntervals = props.timeIntervals.map((item, i) => {
          if (i === index) {
            item.dateRegistration = e.target.value;
            return item;
          }
          return item;
        });
        if (changedTimeIntervals[index].dateRegistration && changedTimeIntervals[index].dateLastActivity
          && changedTimeIntervals[index].dateRegistration > changedTimeIntervals[index].dateLastActivity) {
          props.setDateErrorIndexes([...props.dateErrorIndexes, index]);
        } else {
          props.setDateErrorIndexes(
            props.dateErrorIndexes.filter(item => item !== index),
          )
        }
        props.setTimeIntervals(changedTimeIntervals);
        break;
      case 'dateLastActivity':
        const intervals = props.timeIntervals.map((item, i) => {
          if (i === index) {
            item.dateLastActivity = e.target.value;
          }
          return item;
        });
        if (intervals[index].dateRegistration && intervals[index].dateLastActivity
          && intervals[index].dateRegistration > intervals[index].dateLastActivity) {
          props.setDateErrorIndexes([...props.dateErrorIndexes, index]);
        } else {
          props.setDateErrorIndexes(
            props.dateErrorIndexes.filter(item => item !== index),
          )
        }
        props.setTimeIntervals(intervals);
        break;
      default: break;
    }
  }

  return (
    <div className={classes.timeIntervals}>
      {props.timeIntervals.length > 0 && <div className={classes.timeIntervals__table}>
        <div className={classes.timeIntervals__table__header}>
          <p className={classes.timeIntervals__table__header__userId}>
            UserId
          </p>
          <p className={classes.timeIntervals__table__header__dateRegistration}>
            Date Registration
          </p>
          <p className={classes.timeIntervals__table__header__dateLastActivity}>
            Date Last Activity
          </p>
        </div>
        {props.timeIntervals.map((timeInterval, i) => {
          return (
            <div
              key={i}
              className={classes.timeIntervals__inputs}
            >
              <div className={classes.timeIntervals__inputs__userIdBlock}>
                <input
                  className={props.sameUserIds.includes(timeInterval.userId!) ? classes.timeIntervals__inputs__userIdBlock__err : ''}
                  type="number"
                  onChange={(e) => changeHandler('userId', i, e)}
                  value={timeInterval.userId ?? ''}
                />
              </div>
              <input
                className={props.dateErrorIndexes.includes(i) ? classes.timeIntervals__inputs__userIdBlock__err : ''}
                type="date"
                onChange={(e) => changeHandler('dateRegistration', i, e)}
                value={props.timeIntervals[i].dateRegistration ? props.timeIntervals[i].dateRegistration : ''}
              />
              <input
                className={cn(classes.timeIntervals__inputs__dateLastActivity, {
                  [classes.timeIntervals__inputs__userIdBlock__err]: props.dateErrorIndexes.includes(i)
                }
                )}
                type="date"
                onChange={(e) => changeHandler('dateLastActivity', i, e)}
                value={props.timeIntervals[i].dateLastActivity ? props.timeIntervals[i].dateLastActivity : ''}
              />
              <svg
                onClick={() => removeTimeInterval(i)}
                id="Layer_1"
                data-name="Layer 1"
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 105.16 122.88"
              >
                <title>delete</title>
                <path className="cls-1" d="M11.17,37.16H94.65a8.4,8.4,0,0,1,2,.16,5.93,5.93,0,0,1,2.88,1.56,5.43,5.43,0,0,1,1.64,3.34,7.65,7.65,0,0,1-.06,1.44L94,117.31v0l0,.13,0,.28v0a7.06,7.06,0,0,1-.2.9v0l0,.06v0a5.89,5.89,0,0,1-5.47,4.07H17.32a6.17,6.17,0,0,1-1.25-.19,6.17,6.17,0,0,1-1.16-.48h0a6.18,6.18,0,0,1-3.08-4.88l-7-73.49a7.69,7.69,0,0,1-.06-1.66,5.37,5.37,0,0,1,1.63-3.29,6,6,0,0,1,3-1.58,8.94,8.94,0,0,1,1.79-.13ZM5.65,8.8H37.12V6h0a2.44,2.44,0,0,1,0-.27,6,6,0,0,1,1.76-4h0A6,6,0,0,1,43.09,0H62.46l.3,0a6,6,0,0,1,5.7,6V6h0V8.8h32l.39,0a4.7,4.7,0,0,1,4.31,4.43c0,.18,0,.32,0,.5v9.86a2.59,2.59,0,0,1-2.59,2.59H2.59A2.59,2.59,0,0,1,0,23.62V13.53H0a1.56,1.56,0,0,1,0-.31v0A4.72,4.72,0,0,1,3.88,8.88,10.4,10.4,0,0,1,5.65,8.8Zm42.1,52.7a4.77,4.77,0,0,1,9.49,0v37a4.77,4.77,0,0,1-9.49,0v-37Zm23.73-.2a4.58,4.58,0,0,1,5-4.06,4.47,4.47,0,0,1,4.51,4.46l-2,37a4.57,4.57,0,0,1-5,4.06,4.47,4.47,0,0,1-4.51-4.46l2-37ZM25,61.7a4.46,4.46,0,0,1,4.5-4.46,4.58,4.58,0,0,1,5,4.06l2,37a4.47,4.47,0,0,1-4.51,4.46,4.57,4.57,0,0,1-5-4.06l-2-37Z" />
              </svg>
            </div>
          )
        })}
      </div>}
      <button
        type='button'
        className={classes.timeInterval__addBtn}
        onClick={() => props.setTimeIntervals([{
          id: nanoid(),
          userId: null,
          dateRegistration: '',
          dateLastActivity: '',
        } as ITIMEINTERVAL, ...props.timeIntervals])}
      >
        <svg width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
          <path d="M9 1V17" stroke="#5D6D97" />
          <path d="M17 9L1 9" stroke="#5D6D97" />
        </svg>
        Add interval
      </button>
    </div >
  );
};

export default TimeIntervalList;
