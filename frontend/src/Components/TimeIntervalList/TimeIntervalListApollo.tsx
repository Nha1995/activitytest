import { useMutation, useQuery } from '@apollo/client';
import { nanoid } from 'nanoid';
import React from 'react';
import { HISTOGRAM_DATA, ITIMEINTERVAL, ROLLING_RETENTION } from '../../api/graphqlQueryTypes';
import { CALCULATE_ROLLING__RETENTION, GET_TIME_INTERVALS, SAVE_TIME_INTERVALS } from '../../api/queries';
import { removeTypenames } from '../common/methods';
import TimeIntervalList from './TimeIntervalList';
import classes from './TimeIntervalList.module.scss';
import { Bar } from 'react-chartjs-2';
import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  BarElement,
  Title,
  Tooltip,
  Legend,
} from 'chart.js';

ChartJS.register(
  CategoryScale,
  LinearScale,
  BarElement,
  Title,
  Tooltip,
  Legend
);

const TimeIntervalListApollo = () => {
  const [error, setError] = React.useState(false);
  const [sameUserIds, setSameUserIds] = React.useState<number[]>([]);
  const [dateErrorIndexes, setDateErrorIndexes] = React.useState<number[]>([]);
  const [timeIntervals, setTimeIntervals] = React.useState<ITIMEINTERVAL[]>([{
    id: nanoid(),
    userId: null,
    dateRegistration: '',
    dateLastActivity: '',
  } as ITIMEINTERVAL]);

  const [rollingRetentionOptions, setRollingRetentionOptions] = React.useState<{
    daysCount: number | null,
    result: ROLLING_RETENTION | null,
  }>({ daysCount: null, result: null });

  const [UpdateTimeIntervalInput] = useMutation(SAVE_TIME_INTERVALS, {
    variables: {
      timeIntervals
    },
    fetchPolicy: "network-only",
    onCompleted: () => { }
  });

  const [RollingRetentionData] = useMutation(CALCULATE_ROLLING__RETENTION, {
    variables: {
      daysCount: rollingRetentionOptions.daysCount
    },
    fetchPolicy: "network-only",
    onCompleted: () => { }
  });

  const CalculateRollingRetention = async () => {
    if (rollingRetentionOptions.daysCount) {
      const result = await RollingRetentionData();
      setRollingRetentionOptions({
        ...rollingRetentionOptions, result: {
          result: result.data.calculateRollingRetention.result,
          histogramData: result.data.calculateRollingRetention.histogramData
        }
      })
    }
  }

  const { data } = useQuery(GET_TIME_INTERVALS, {
    fetchPolicy: "network-only",
    onCompleted: () => {
      if (data) {
        const mappedData = removeTypenames(data.timeIntervals);
        if (mappedData.length === 0) {
          setTimeIntervals([{
            id: nanoid(),
            userId: null,
            dateRegistration: '',
            dateLastActivity: '',
          } as ITIMEINTERVAL]);
        } else {
          setTimeIntervals([...mappedData]);
        }
      }
    }
  });

  const save = () => {
    let isError = false;
    const checkTimeIntervals = timeIntervals.filter((item) => {
      if (!item.userId && !item.dateRegistration && !item.dateLastActivity) {
        return false;
      }
      if (!item.userId || !item.dateRegistration || !item.dateLastActivity) {
        setError(true);
        isError = true;
      }
      return true;
    })
    if (!isError) {
      setError(false);
      if (sameUserIds.length === 0 && dateErrorIndexes.length === 0) {
        UpdateTimeIntervalInput({ variables: { timeIntervals: checkTimeIntervals } });
        alert("Данные успешно сохранены!");
      }
    }
  }

  const options = {
    responsive: true,
    plugins: {
      legend: {
        position: 'top' as const,
      },
      title: {
        display: true,
        text: '',
      },
    },
  };

  return (
    <div className={classes.timeIntervalApollo}>
      <TimeIntervalList
        timeIntervals={timeIntervals}
        setTimeIntervals={setTimeIntervals}
        sameUserIds={sameUserIds}
        setSameUserIds={setSameUserIds}
        dateErrorIndexes={dateErrorIndexes}
        setDateErrorIndexes={setDateErrorIndexes}
      />
      <div className={classes.timeIntervalApollo__BtnAndErrMessageBlock}>
        <div>
          {dateErrorIndexes.length > 0 && <p>*Дата последнего посещения не может быть раньше даты регистрации</p>}
          {error && <p>*Не все интервалы заполнены корректно</p>}
        </div>
        <button
          className={classes.timeIntervalApollo__btn}
          onClick={() => save()}
        >
          Save
        </button>
      </div>
      <div className={classes.timeIntervalApollo__RR_block}>
        <p>
          Rolling Retention
          <input
            onChange={(e) => e ? setRollingRetentionOptions({ ...rollingRetentionOptions, daysCount: +e.target.value })
              : setRollingRetentionOptions({ ...rollingRetentionOptions, daysCount: null })}
            type="number"
          />
          day =
          <span>{(isNaN(+rollingRetentionOptions.result?.result!)) ? 0 : rollingRetentionOptions.result!.result}%</span>
        </p>
        <button
          onClick={() => CalculateRollingRetention()}
          className={classes.timeIntervalApollo__btn}
        >
          Calculate
        </button>
      </div>
      <div className={classes.timeIntervalApollo__histogram}>
        {rollingRetentionOptions.result?.result && (
          <Bar options={options} data={{
            labels: rollingRetentionOptions.result.histogramData.map(item => item.year),
            datasets: [{
              backgroundColor: 'aaa',
              data: rollingRetentionOptions.result.histogramData.map(item => item.value),
              label: 'Users'
            }]
          }} />
        )}
      </div>
    </div>
  );
};

export default TimeIntervalListApollo;
