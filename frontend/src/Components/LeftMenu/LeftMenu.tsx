import React from 'react';
import classes from './LeftMenu.module.scss';

const LeftMenu = () => {
  return (
    <div className={classes.leftMenu}>
      <p>Some link 1</p>
      <p>Some link 2</p>
      <p>Some link 3</p>
      <p>Some link 4</p>
    </div>
  );
};

export default LeftMenu;
