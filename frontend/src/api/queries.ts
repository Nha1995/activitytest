import { gql } from '@apollo/client';

export const GET_TIME_INTERVALS = gql`
    query GET{
      timeIntervals {
		  id
      userId
      dateRegistration
      dateLastActivity
    }
}
`;

export const SAVE_TIME_INTERVALS = gql`
  mutation SAVE_TIME_INTERVALS($timeIntervals: [UpdateTimeIntervalInput]!) {
    updateTimeIntervals(updateTimeIntervalsInputs: $timeIntervals) {
      __typename
      id
      userId
      dateRegistration
      dateLastActivity
    }
  }
`;

export const CALCULATE_ROLLING__RETENTION = gql`
mutation CALCULATE_RR($daysCount: Int!) {
  calculateRollingRetention(daysCount: $daysCount){
    result
    histogramData {
      year
      value
    }
  }
}
`; 