export interface ITIMEINTERVAL {
  id: string
  userId: number | null
  dateRegistration: string
  dateLastActivity: string
}

export interface ROLLING_RETENTION {
  result: string;
  histogramData: HISTOGRAM_DATA[];
}

export interface HISTOGRAM_DATA {
  year: number;
  value: number;
}
