import React from 'react';
import classes from './App.module.scss';
import './App.css';
import Header from './Components/Header/Header';
import LeftMenu from './Components/LeftMenu/LeftMenu';
import TimeIntervalListApollo from './Components/TimeIntervalList/TimeIntervalListApollo';

function App() {
  return (
    <div className={classes.App}>
      <Header />
      <div className={classes.App__body}>
        <LeftMenu />
        <TimeIntervalListApollo />
      </div>
    </div>
  );
}

export default App;

