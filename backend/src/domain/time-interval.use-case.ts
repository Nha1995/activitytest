import { Injectable } from '@nestjs/common';
import { plainToClass } from 'class-transformer';
import { TimeInterval } from 'src/infrastructure/model/time-interval.model';
import { TimeIntervalRepo } from 'src/infrastructure/time-interval.repo';
import { HistogramData, RollingRetentionData } from 'src/presentation/input/rolling-retention-data';
import { TimeIntervalDTO } from 'src/presentation/input/time-interval-dto';
import { UpdateTimeIntervalInput } from 'src/presentation/input/update-time-interval.input';
import { CreateTimeIntervalInput } from '../presentation/input/create-time-interval.input';

@Injectable()
export class TimeIntervalUseCase {
  constructor(private readonly timeIntervalRepo: TimeIntervalRepo) { }

  public async getAll(): Promise<CreateTimeIntervalInput[]> {
    const timeIntervalsDbModels = await this.timeIntervalRepo.getAll();
    const mappedTimeIntevals = timeIntervalsDbModels.map((item) => {
      return new TimeIntervalDTO(
        item.id,
        item.userId,
        this.dateToString(item.dateRegistration),
        this.dateToString(item.dateLastActivity),
      )
    });
    return mappedTimeIntevals;
  }

  public async calculateRollingRetention(daysCount: number): Promise<RollingRetentionData> {
    const allTimeIntervals = await this.timeIntervalRepo.getAll();
    var returned = 0;
    var previouslyRegistered = 0;
    for (const interval of allTimeIntervals) {
      if ((interval.dateLastActivity.getTime() - interval.dateRegistration.getTime()) / 86400000 >= daysCount) {
        returned++;
      }
      if (this.difference(interval.dateRegistration, new Date(), daysCount)) {
        previouslyRegistered++;
      }
    }
    const result = ((returned / previouslyRegistered) * 100).toFixed(2);

    const years = [2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019, 2020, 2021, 2022];
    const timeIntervals = await this.timeIntervalRepo.getAll();

    const data = years.map((year) => {
      var usersCount = 0;
      const timeIntervalsByYear = timeIntervals.filter((item) => item.dateLastActivity.getFullYear() === year);
      for (const interval of timeIntervalsByYear) {
        if (this.difference(interval.dateRegistration, interval.dateLastActivity, daysCount)) {
          usersCount++;
        }
      }
      return {
        year: year,
        value: usersCount,
      } as HistogramData
    });
    return new RollingRetentionData(result, data);
  }

  public async create(
    createTimeIntervalInput: CreateTimeIntervalInput[],
  ): Promise<TimeInterval[]> {
    const mappedTimeIntervals = createTimeIntervalInput.map((item) => {
      return new TimeInterval(item.userId, this.getDate(item.dateRegistration), this.getDate(item.dateLastActivity))
    });
    return this.timeIntervalRepo.save(mappedTimeIntervals);
  }

  public async save(
    updateTimeIntervalInput: UpdateTimeIntervalInput[],
  ): Promise<TimeInterval[]> {
    const plainedTimeIntervals = plainToClass(TimeInterval, updateTimeIntervalInput);
    const existingTimeIntervals = await this.timeIntervalRepo.getAll();

    const removedTimeIntervalIds = existingTimeIntervals.filter(timeInterval => !plainedTimeIntervals.includes(timeInterval));
    await this.remove(removedTimeIntervalIds.map(item => item.id));

    return this.timeIntervalRepo.save(plainedTimeIntervals);
  }

  public async remove(ids: string[]): Promise<TimeInterval[]> {
    const timeIntervals = await this.timeIntervalRepo.getByIds(ids);
    return this.timeIntervalRepo.remove(timeIntervals);
  }

  private getDate(stringDate: string): Date {
    const splitedString = stringDate.split('-');
    return new Date(+splitedString[0], +splitedString[1], +splitedString[2]);
  }

  private dateToString = (date: Date): string => {
    const day = date.getDate() < 10 ? `0${date.getDate()}` : date.getDate();
    const month = date.getMonth() + 1 < 10
      ? `0${date.getMonth() + 1}` : date.getMonth() + 1;
    return `${date.getFullYear()}-${month}-${day}`;
  };

  private difference(dateRegistration: Date, dateLastActivity: Date, days: number): boolean {
    return Math.abs((+dateRegistration - +dateLastActivity) / 86400000) >= days;
  }
}
