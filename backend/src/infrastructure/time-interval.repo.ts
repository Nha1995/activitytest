import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { In, LessThan, MoreThan, Repository } from 'typeorm';
import { TimeInterval } from './model/time-interval.model';

@Injectable()
export class TimeIntervalRepo {
  constructor(
    @InjectRepository(TimeInterval)
    private readonly timeIntervalRepo: Repository<TimeInterval>,
  ) { }

  public async save(timeIntervals: TimeInterval[]): Promise<TimeInterval[]> {
    return this.timeIntervalRepo.save(timeIntervals);
  }

  public async getAll(): Promise<TimeInterval[]> {
    return this.timeIntervalRepo.find();
  }

  public async getByIds(ids: string[]): Promise<TimeInterval[]> {
    const allTimeIntervals = await this.timeIntervalRepo.find();
    const findTimeIntervals = allTimeIntervals.filter((timeInterval) => {
      return ids.includes(timeInterval.id);
    })
    return findTimeIntervals;
  }

  public async remove(timeIntervals: TimeInterval[]): Promise<TimeInterval[]> {
    return this.timeIntervalRepo.remove(timeIntervals);
  }
}
