import { Column, Entity, Index, PrimaryColumn } from 'typeorm';

@Entity()
export class TimeInterval {
  constructor(userId: number, dateRegistration: Date, dateLastActivity: Date) {
    if (userId && dateRegistration && dateLastActivity) {
      this.userId = userId;
      this.dateRegistration = dateRegistration;
      this.dateLastActivity = dateLastActivity;
    }
  }

  @PrimaryColumn()
  @Index({ unique: true })
  id: string;

  @PrimaryColumn()
  @Index({ unique: true })
  userId: number;

  @Column()
  dateRegistration: Date;

  @Column()
  dateLastActivity: Date;
}
