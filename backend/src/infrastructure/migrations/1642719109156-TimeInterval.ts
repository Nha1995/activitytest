import {MigrationInterface, QueryRunner} from "typeorm";

export class TimeInterval1642719109156 implements MigrationInterface {
    name = 'TimeInterval1642719109156'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "time_interval" ("id" character varying NOT NULL, "userId" integer NOT NULL, "dateRegistration" TIMESTAMP NOT NULL, "dateLastActivity" TIMESTAMP NOT NULL, CONSTRAINT "PK_c61cabb70016133b6c1235c55fa" PRIMARY KEY ("id", "userId"))`);
        await queryRunner.query(`CREATE UNIQUE INDEX "IDX_58103fb78a0665fd4af112c5e2" ON "time_interval" ("id") `);
        await queryRunner.query(`CREATE UNIQUE INDEX "IDX_83ed3cc5c025f7212966ea0a23" ON "time_interval" ("userId") `);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP INDEX "public"."IDX_83ed3cc5c025f7212966ea0a23"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_58103fb78a0665fd4af112c5e2"`);
        await queryRunner.query(`DROP TABLE "time_interval"`);
    }

}
