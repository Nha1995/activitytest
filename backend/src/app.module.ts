import { Module } from '@nestjs/common';
import { GraphQLModule } from '@nestjs/graphql';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TimeIntervalUseCase } from './domain/time-interval.use-case';
import { TimeInterval } from './infrastructure/model/time-interval.model';
import { TimeIntervalRepo } from './infrastructure/time-interval.repo';
import { TimeIntervalResolver } from './presentation/resolver/time-interval.resolver';

@Module({
  imports: [
    GraphQLModule.forRoot({
      typePaths: ['./**/*.graphql'],
      playground: true,
    }),
    TypeOrmModule.forRoot({
      entities: [TimeInterval],
    }),
    TypeOrmModule.forFeature([TimeInterval]),
  ],
  providers: [TimeIntervalRepo, TimeIntervalResolver, TimeIntervalUseCase],
})
export class AppModule {}
