import { CreateTimeIntervalInput } from './create-time-interval.input';
import { PartialType } from '@nestjs/mapped-types';

export class UpdateTimeIntervalInput extends PartialType(
  CreateTimeIntervalInput,
) {
  public id: string;

  public userId: number;

  public dateRegistration: string;

  public dateLastActivity: string;
}
