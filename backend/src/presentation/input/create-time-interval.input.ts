export class CreateTimeIntervalInput {
  public id: string;

  public userId: number;

  public dateRegistration: string;

  public dateLastActivity: string;
}
