export class TimeIntervalDTO {
  constructor(id: string, userId: number, dateRegistration: string, dateLastActivity: string) {
    if (id && userId && dateRegistration && dateLastActivity) {
      this.id = id;
      this.userId = userId;
      this.dateRegistration = dateRegistration;
      this.dateLastActivity = dateLastActivity;
    }
  }

  public id: string;

  public userId: number;

  public dateRegistration: string;

  public dateLastActivity: string;
}
