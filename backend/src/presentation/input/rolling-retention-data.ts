export class RollingRetentionData {

  constructor(result: string, histogramData: HistogramData[]) {
    this.result = result;
    this.histogramData = histogramData;
  }

  result: string;
  histogramData: HistogramData[];
}

export interface HistogramData {
  year: number;
  value: number;
}
