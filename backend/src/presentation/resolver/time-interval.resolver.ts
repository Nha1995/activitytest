import { Resolver, Query, Mutation, Args } from '@nestjs/graphql';
import { TimeIntervalUseCase } from '../../domain/time-interval.use-case';
import { CreateTimeIntervalInput } from '../input/create-time-interval.input';
import { UpdateTimeIntervalInput } from '../input/update-time-interval.input';

@Resolver('TimeInterval')
export class TimeIntervalResolver {
  constructor(private readonly timeIntervalUseCase: TimeIntervalUseCase) { }

  @Query('timeIntervals')
  public async findAll(): Promise<CreateTimeIntervalInput[]> {
    return await this.timeIntervalUseCase.getAll();
  }

  @Mutation('calculateRollingRetention')
  public async calculateRollingRetention(
    @Args('daysCount')
    daysCount: number
  ) {
    return this.timeIntervalUseCase.calculateRollingRetention(daysCount);
  }

  @Mutation('createTimeIntervals')
  public async create(
    @Args('createTimeIntervalsInputs')
    createTimeIntervalsInputs: CreateTimeIntervalInput[],
  ) {
    return this.timeIntervalUseCase.create(createTimeIntervalsInputs);
  }

  @Mutation('updateTimeIntervals')
  public async save(
    @Args('updateTimeIntervalsInputs')
    updateTimeIntervalInputs: UpdateTimeIntervalInput[],
  ) {
    return this.timeIntervalUseCase.save(updateTimeIntervalInputs);
  }

  @Mutation('removeTimeIntervals')
  public async removeTimeIntervals(
    @Args('ids')
    ids: string[],
  ) {
    return this.timeIntervalUseCase.remove(ids);
  }
}
